﻿using MeRestaurantProj.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeRestaurantProj.Controllers
{
    public class OrderController : Controller
    {
        private readonly ApplicationContext _context;

        public OrderController(ApplicationContext context)
        {
            _context = context;
        }

        public IActionResult AddToBucket([Bind("Name,Price")] Dish dish)
        {
            _context.Orders.Add(new Order { Dishes = { dish } });
            return RedirectToAction("Index");
        }
    }
}
