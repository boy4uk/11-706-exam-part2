﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeRestaurantProj.Models
{
    public class Order
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public List<Dish> Dishes { get; set; }

        public Order()
        {
            Dishes = new List<Dish>();
        }
    }
}
